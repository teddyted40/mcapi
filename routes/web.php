<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "Teddys Movie Collection API - " . $router->app->version();
});

//$router->get('/movies', 'MovieController@index');
//$app->get('profile', [
//    'as' => 'profile', 'uses' => 'UserController@showProfile'
//]);

$router->group(['prefix'=>'api/v1'], function() use($router){
$router->get('/movies', 'MovieController@index');
$router->post('/movie', 'MovieController@create');
$router->get('/movie/{id}', 'MovieController@show');
$router->put('/movie/{id}', 'MovieController@update');
$router->delete('/movie/{id}', 'MovieController@destroy');
});


/*$router->post('/movie', function (Request $request) {
    $this->validate($request, [
        'title' => 'required|unique:movies',
	'format' => 'required',
	'length' => 'required',
	'released' => 'required',
	'rating' => 'required',
    ]);
});
 */
