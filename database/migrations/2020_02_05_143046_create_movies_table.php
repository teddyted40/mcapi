<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
		$table->bigIncrements('id');
		$table->string('title');
		$table->string('format'); //@TODO enum -- allowable values “VHS”, “DVD”, “Streaming”
		$table->integer('length'); //value between 0 and 500 minutes
		$table->integer('released'); //value between 1800 and 2100
		$table->integer('rating'); // value between 1 and 5
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
