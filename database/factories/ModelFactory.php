<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Movie;

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
	'format' => "DVD",
	'length' => rand(0,500),
	'released' => rand(1800,2100),
	'rating' => rand(1,5),
    ];
});
