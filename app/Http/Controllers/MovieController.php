<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;

class MovieController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $movies = Movie::all();
        return response()->json($movies);
    }

    /**
     * Creates a new movie
     * @param  Request  $request
     * @return Response
     */
    public function create(\Illuminate\Http\Request $request)
    {
	$uri = $request->path();
	
	$data = $request->all(); 
	
	$movie = new Movie;
	$movie->title    = $data["title"];
	$movie->format   = $data["format"];
	$movie->length   = $data["length"];
	$movie->released = $data["released"];
	$movie->rating   = $data["rating"];

        $movie->save();

	return response()->json($movie);
    }

    /**
     * Show specific movie details to the user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $movie = Movie::find($id);

        return response()->json($movie);
    }

    /**
     * Update movie information
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
     public function update(\Illuminate\Http\Request $request, $id)
     { 
	//var_dump($id);exit;
        $movie = Movie::find($id);

	$data = $request->all();

        $movie->title    = $data['title'];
        $movie->format   = $data['format'];
        $movie->length   = $data['length'];
        $movie->released = $data['released'];
	$movie->rating   = $data['rating'];

        $movie->save();
        return response()->json($movie);
     }

    /**
     * Delete movie
     *
     * @param  int  $id
     * @return Response
     */
     public function destroy($id)
     {
        $movie = Movie::find($id);
        $movie->delete();

         return response()->json('Movie deleted successfully');
     }

}
